
Crafty.sprite( 400, 96, "img/UI_Panel0.1.png", {
	UI_Panel: [0,0] });

Crafty.sprite( 16, "img/UI_Icons0.12.png", {
	icon_game: [0,0],
	icon_game_A: [1,0],
	icon_shop: [2,0],
	icon_shop_A: [3,0],
	icon_stop: [0,1],
	icon_stop_A: [1,1],
	icon_play1: [2,1],
	icon_play1_A: [3,1],
	icon_play2: [0,2],
	icon_play2_A: [1,2],
	icon_play3: [2,2,2,1],
	icon_play3_A: [0,3,2,1],
	icon_toggle: [2,3,1,2],
	icon_toggle_A: [3,3,1,2],
	sp_gold: [4,0],
	sp_inv_box_left: [0,4],
	sp_inv_box_right: [1,4],
	sp_inv_box: [5,0]
	});

Crafty.sprite( 16, "img/Items0.1.png", {
	sp_bowl_full: [0,0],
	sp_bowl_empty: [1,0],
	sp_bowl_full_A: [2,0],
	sp_bowl_empty_A: [3,0],
	sp_doghouse: [4,0],
	sp_doghouse_A: [5,0],
	sp_stick: [6,0],
	sp_stick_A: [7,0],
	sp_fire1: [0,1],
	sp_fire2: [1,1],
	sp_fire3: [2,1],
	sp_fire_A: [3,1]
});

Crafty.sprite( 22, "img/UI_CategoryIcons0.1.png", {
	sp_category_food: [0,0],
	sp_category_food_A: [1,0],
	sp_category_home: [2,0],
	sp_category_home_A: [3,0],
	sp_category_fun: [0,1],
	sp_category_fun_A: [1,1],
	sp_category_health: [2,1],
	sp_category_health_A: [3,1]
});


	var gameBtn;
	var shopBtn;

	var cur_sub_UI = "play";

	var G_SPEED_PREV = 1;
	var G_SPEED_STATE = 1;	//0 stop, 1 play, 2 2x, 3 3x
	var G_SPEED_FACTOR = 1.0;
	var SPEED_PLAY1 = 1.0;
	var SPEED_PLAY2 = 2.0;
	var SPEED_PLAY3 = 3.0;

	var CLOCK_LABEL=null;
	var CLOCK_MIN = 50;	//minutes
	var CLOCK_H = 23;	//hours

function rescaleAnimations() {
	var entities = Crafty("SpriteAnimation");
	//console.log("Length ",entities.length);
	for(e=0; e<entities.length; e++) {
		if(G_SPEED_STATE!=0)
			entities.get(e).animationSpeed = G_SPEED_FACTOR;
	}
}


var playerGoldLabel=null;
/////////////////////////Load Main Bottom UI///////////////////////////
function loadUI() {
	console.log("INIT UI");
	
	Crafty.e("2D, Canvas, UI_Panel")
		.attr({x: 0, y: WINDOW_H-96, z: Z_UI_PANEL});
		
	gameBtn = Crafty.e("2D, DOM, Mouse, icon_game_A")
		.attr({x: 8, y: WINDOW_H-96+6, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			this.sprite(1,0,1,1);
		}).bind("MouseOut", function() {
			if(cur_sub_UI!="play")
				this.sprite(0,0,1,1);
		}).bind("Click", function() {
			//activate self, and deactivate the other
			this.sprite(1,0,1,1);
			shopBtn.sprite(2,0,1,1);
			if( cur_sub_UI != "play" ) {
				leaveShoppingUI();
				setPlayingUI();
			}
		});
		
	shopBtn = Crafty.e("2D, DOM, Mouse, icon_shop")
		.attr({x: 8+32, y: WINDOW_H-96+6, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			this.sprite(3,0,1,1);
		}).bind("MouseOut", function() {
			if(cur_sub_UI!="shop")
			this.sprite(2,0,1,1);
		}).bind("Click", function() {
			//activate self, and deactivate the other
			this.sprite(3,0,1,1);
			
			gameBtn.sprite(0,0,1,1);
			if( cur_sub_UI != "shop" ) {
				leavePlayingUI();
				setShoppingUI();
			}
		});

	var INV_BOX_OFFS_X = 12;
	var INV_BOX_OFFS_Y = WINDOW_H-96*0.4-4;
	
	Crafty.e("2D, DOM, Mouse, SpriteAnimation, sp_gold")
		.attr({ x: INV_BOX_OFFS_X, y:INV_BOX_OFFS_Y, z: Z_UI_ITEM})
		.reel("roll", 500, [[4,0], [4,1], [4,2], [4,3], [4,4]])
		.bind("MouseOver", function() {
			this.animate("roll", -1);
		})
		.bind("MouseOut", function() {
			this.pauseAnimation();
			this.resetAnimation();
		});
		
	var textbox_w = 45;
	playerGoldLabel = Crafty.e("2D, DOM, Text").attr({ x: INV_BOX_OFFS_X+58+1-textbox_w, y:INV_BOX_OFFS_Y+1, z: Z_UI_ITEM, w:textbox_w, h:16})
		.css({"text-align": "right"})
		.text(playerGold)
		.textFont({ family: 'Times New Roman' })
		.textColor('#000000')
		.textFont({ size: '12px', weight: 'bold' })
		.unselectable();
	/*Crafty.e("2D, DOM, Text").attr({ x: INV_BOX_OFFS_X+16, y:INV_BOX_OFFS_Y, z: Z_UI_ITEM})
		.text("$14123242")
		.textFont({ family: 'Times New Roman' })
		.textColor('#FDD017')
		.textFont({ size: '12px' , weight: 'bold' })
		.unselectable();*/
	
	
	Crafty.e("2D, DOM, Color")
		.attr({ x: INV_BOX_OFFS_X, y:INV_BOX_OFFS_Y, z: Z_UI_ITEM-2, w:64, h: 16})
		.color(RGB_UI_BASE);
	Crafty.e("2D, DOM, sp_inv_box_left")
		.attr({ x: INV_BOX_OFFS_X, y:INV_BOX_OFFS_Y, z: Z_UI_ITEM-1});
	Crafty.e("2D, DOM, sp_inv_box")
		.attr({ x: INV_BOX_OFFS_X+16, y:INV_BOX_OFFS_Y, z: Z_UI_ITEM-1});
	Crafty.e("2D, DOM, sp_inv_box")
		.attr({ x: INV_BOX_OFFS_X+32, y:INV_BOX_OFFS_Y, z: Z_UI_ITEM-1});
	//Crafty.e("2D, DOM, sp_inv_box")
		//.attr({ x: INV_BOX_OFFS_X+48, y:INV_BOX_OFFS_Y, z: Z_UI_ITEM-1});
	Crafty.e("2D, DOM, sp_inv_box_right")
		.attr({ x: INV_BOX_OFFS_X+48, y:INV_BOX_OFFS_Y, z: Z_UI_ITEM-1});
		
		
	
	var SPEED_OFFSET_X = 55;
	
	Crafty.e("2D, DOM,  Mouse, icon_stop")
		.attr({x: 8+SPEED_OFFSET_X, y: WINDOW_H-6-16, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			this.sprite(1,1,1,1);
		}).bind("MouseOut", function() {
			if(G_SPEED_STATE!=0)
				this.sprite(0,1,1,1);
		}).bind("Click", function() {			
			if(!Crafty.isPaused()) {
				this.sprite(1,1,1,1);
				G_SPEED_STATE = 0;
				this.bind("EnterFrame", function() {
					this.unbind("EnterFrame");
					Crafty.pause();	//pause
				});
			} else {
				this.paused = false;
				G_SPEED_STATE = 1;
				G_SPEED_FACTOR = SPEED_PLAY1;
				icon_play1.sprite(3,1,1,1);
				Crafty.pause();	//resume
			}
			Crafty.trigger("SpeedBtnClick");
		}).bind("SpeedBtnClick", function() {
			if(G_SPEED_STATE!=0) {
				this.sprite(0,1,1,1);
				rescaleAnimations();
			}
		});
		
	var icon_play1 = Crafty.e("2D, DOM,  Mouse, icon_play1_A")
		.attr({x: 8+16+2+SPEED_OFFSET_X, y: WINDOW_H-6-16, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			this.sprite(3,1,1,1);
		}).bind("MouseOut", function() {
			if(G_SPEED_STATE!=1)
				this.sprite(2,1,1,1);
		}).bind("Click", function() {
			G_SPEED_STATE = 1;
			G_SPEED_FACTOR = SPEED_PLAY1;
			this.sprite(3,1,1,1);
			Crafty.trigger("SpeedBtnClick");
		}).bind("SpeedBtnClick", function() {
			if(G_SPEED_STATE!=1)
				this.sprite(2,1,1,1);
		});
	var icon_play2 = Crafty.e("2D, DOM,  Mouse, icon_play2")
		.attr({x: 8+2*16+4+SPEED_OFFSET_X, y: WINDOW_H-6-16, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			this.sprite(1,2,1,1);
		}).bind("MouseOut", function() {
			if(G_SPEED_STATE!=2)
				this.sprite(0,2,1,1);
		}).bind("Click", function() {
			G_SPEED_STATE = 2;
			G_SPEED_FACTOR = SPEED_PLAY2;
			this.sprite(1,2,1,1);
			Crafty.trigger("SpeedBtnClick");
		}).bind("SpeedBtnClick", function() {
			if(G_SPEED_STATE!=2)
				this.sprite(0,2,1,1);
		});
	var icon_play3 = Crafty.e("2D, DOM,  Mouse, icon_play3")
		.attr({x: 8+3*16+1+SPEED_OFFSET_X, y: WINDOW_H-6-16, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			this.sprite(0,3,2,1);
		}).bind("MouseOut", function() {
			if(G_SPEED_STATE!=3)
				this.sprite(2,2,2,1);
		}).bind("Click", function() {
			G_SPEED_STATE = 3;
			G_SPEED_FACTOR = SPEED_PLAY3;
			this.sprite(0,3,2,1);
			Crafty.trigger("SpeedBtnClick");
		}).bind("SpeedBtnClick", function() {
			if(G_SPEED_STATE!=3)
				this.sprite(2,2,2,1);
		});

	Crafty.e("2D,  Mouse, DOM, icon_toggle")
		.attr({x: 278, y: WINDOW_H-96/2-16, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			this.sprite(3,3,1,2);
		}).bind("MouseOut", function() {
			this.sprite(2,3,1,2);
		}).bind("Click", function() {
		});
		
	var _text_w = 60;
	var _text_h = 15;
		
	var _btn_w = 62;
	var _btn_h = 15;
		
	Crafty.e("2D, DOM, Mouse, Color")
		.attr({w: _btn_w, h: _btn_h, x: 14, y: WINDOW_H-96*0.6-_btn_h/2, z: Z_UI_ITEM})
		.color(RGB_UI_BUTTON_DEFAULT)
		.bind("Click", function() {
			//Crafty.scene("MainMenu");
		})
		.bind("MouseOver", function(){
			this.color(RGB_UI_BUTTON_ACTIVE);
		})
		.bind("MouseOut", function(){
			this.color(RGB_UI_BUTTON_DEFAULT);
		});
		
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: 20, y: WINDOW_H-96*0.6-_text_h/2, z: Z_UI_ITEM})
		.text(S_BACK_TO_MENU)
		.textFont({ family: 'Times New Roman' })
		.textColor(RGB_UI_TEXT)
		.textFont({ size: '12px', weight: 'bold' })
		.unselectable();
	
	
	Crafty.e("2D, DOM, Color")
		.attr({w: 35, h: _text_h, x: 16+2, y: WINDOW_H-96*0.15-_text_h/2, z: Z_UI_ITEM-1})
		.color(RGB_UI_BASE);
	Crafty.e("2D, DOM, Color")
		.attr({w: 35+2, h: _text_h+2, x: 16+2-1, y: WINDOW_H-96*0.15-_text_h/2-1, z: Z_UI_ITEM-2})
		.color('#000000');
		
		var _compat;
		var _pre;
		if(CLOCK_MIN<10)
			_compat = ":0";
		else _compat = ":";
		
		if(CLOCK_H<10)
			_pre = "_";
		else
			_pre = "";
	CLOCK_LABEL = Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: 16+5, y: WINDOW_H-96*0.15-_text_h/2+1, z: Z_UI_ITEM})
		.text(_pre + CLOCK_H.toString() + _compat + CLOCK_MIN.toString())
		.textFont({ family: 'Times New Roman' })
		.textColor(RGB_UI_TEXT)
		.textFont({ size: '12px', weight: 'bold' })
		.unselectable();
		
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: 126-_text_w/2, y: WINDOW_H-96+6, z: Z_UI_ITEM})
		.text(S_SIMS)
		.textFont({ family: 'Times New Roman' })
		.textColor(RGB_UI_TEXT)
		.css({"text-align": "center"})
		.textFont({ size: '12px', weight: 'bold' })
		.unselectable();

	setPlayingUI();
	
	//Crafty.e("ActionMenu").setParam( 100, 200, 3, "hello", 1);
	
	Crafty.e("Delay").delay(function() {
		CLOCK_MIN += G_SPEED_FACTOR;
		if(CLOCK_MIN>=60) {
			CLOCK_MIN=0;
			CLOCK_H += 1;
			if(CLOCK_H>=24)
				CLOCK_H = 0;
		}
		var compat;
		var pre;
		if(CLOCK_MIN<10)
			compat = ":0";
		else compat = ":";
		
		if(CLOCK_H<10)
			pre = "_";
		else
			pre = "";
			
		CLOCK_LABEL.text(pre + CLOCK_H.toString() + compat + CLOCK_MIN.toString());
	}, 1000, -1);
}
/////////////////////////////////////////////////////////////////////

//////////////////////Set Game/Playing UI/////////////////////////////

	var NEEDBAR_W = 40;
	var NEEDBAR_H = 4;
//////////NEEDBAR COMPONENT////////////	
Crafty.c("NeedBar", {

	init: function() {
		this.addComponent("2D, DOM, Color, Mouse");
		this.percent = 0.65;
		this.color('#00FF00');
		this.redPart = Crafty.e("2D, DOM, Color, Mouse");
		this.redPart.color('#FF0000');
		this.bind("Remove", function() {
			this.redPart.destroy();
		});
		this.bind("MouseOver", function() {
			this.sel=Crafty.e("2D, DOM, Color")
				.attr({ x: this._x-1, y: this._y-1, z: this._z-2, w: NEEDBAR_W+2, h: this._h+2})
				.color('#FFFF00')
		});
		this.bind("MouseOut", function() {
			this.sel.destroy();
		});

		this.redPart.bind("MouseOver", function() {
			this.sel2=Crafty.e("2D, DOM, Color")
				.attr({ x: this._x-1, y: this._y-1, z: this._z-2, w: NEEDBAR_W+2, h: this._h+2})
				.color('#FFFF00')
		});
		this.redPart.bind("MouseOut", function() {
			this.sel2.destroy();
		});
	},
	
	setParam: function( leftX, midY) {
		this.attr( { x: leftX, y: midY-NEEDBAR_H/2, z: Z_UI_ITEM, w: NEEDBAR_W*this.percent, h: NEEDBAR_H} );
		this.redPart.attr({ x: leftX, y: midY-NEEDBAR_H/2, z: Z_UI_ITEM-1, w: NEEDBAR_W, h: NEEDBAR_H  });
	},
	
	setPercent: function(p) {
		if(p<0.0) p = 0.0;
		if(p>1.0) p = 1.0;
		this.percent = p;
		this.attr( { w:  Math.round(NEEDBAR_W*p-0.5) } );
	}
	
});
///////////////////////////////////////

var playElements = new Array();

function setPlayingUI() {

	cur_sub_UI = "play";
	var _text_w = 60;
	var _text_h = 15;

	var ent = Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: 215-_text_w/2, y: WINDOW_H-96+6, z: Z_UI_ITEM})
		.text(S_NEEDS)
		.textFont({ family: 'Times New Roman' })
		.textColor(RGB_UI_TEXT)
		.css({"text-align": "center"})
		.textFont({ size: '12px', weight: 'bold' })
		.unselectable();
		
	playElements.push(ent);

	ent = Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: 335-_text_w/2, y: WINDOW_H-96+6, z: Z_UI_ITEM})
		.text(S_ATTRIBUTES)
		.textFont({ family: 'Times New Roman' })
		.textColor(RGB_UI_TEXT)
		.css({"text-align": "center"})
		.textFont({ size: '12px', weight: 'bold' })
		.unselectable();

	playElements.push(ent);

	//var bar = Crafty.e("NeedBar");
	//bar.setParam( 175, WINDOW_H-96+96*0.3);
	/*Crafty.e("Delay").delay(function() {
		bar.setPercent(bar.percent+0.01);
	}, 250, -1);
	*/
	//playElements.push(bar);
	
	var NEEDBAR_OFFSETX = 170;
	var NEEDBAR_OFFSETY = WINDOW_H-96+96*0.375;
	var NEEDBAR_TEXT_X = NEEDBAR_OFFSETX-5;
	var NEEDBAR_TEXT_Y = NEEDBAR_OFFSETY-13;
	var NEEDBAR_STEPX = 54;
	var NEEDVAR_STEPY = 15;
	
	
	var lab = Crafty.e("2D, DOM, Text")
		.attr({ x: NEEDBAR_TEXT_X, y: NEEDBAR_TEXT_Y, z:Z_UI_ITEM+1})
		.textColor(RGB_UI_TEXT)
		.text(S_BAR_ENERGY)
	var bar = Crafty.e("NeedBar");
	bar.setParam( NEEDBAR_OFFSETX, NEEDBAR_OFFSETY);
	playElements.push(bar);
	playElements.push(lab);
	bar_energy = bar;
	
	lab = Crafty.e("2D, DOM, Text")
		.attr({ x: NEEDBAR_TEXT_X+NEEDBAR_STEPX, y: NEEDBAR_TEXT_Y, z:Z_UI_ITEM+1})
		.textColor(RGB_UI_TEXT)
		.text(S_BAR_TEMP)
	bar = Crafty.e("NeedBar");
	bar.setParam( NEEDBAR_OFFSETX+NEEDBAR_STEPX, NEEDBAR_OFFSETY);
	playElements.push(bar);
	playElements.push(lab);
	bar_temp = bar;
	
	lab = Crafty.e("2D, DOM, Text")
		.attr({ x: NEEDBAR_TEXT_X, y: NEEDBAR_TEXT_Y+NEEDVAR_STEPY, z:Z_UI_ITEM+1})
		.textColor(RGB_UI_TEXT)
		.text(S_BAR_HUNGER)
	bar = Crafty.e("NeedBar");
	bar.setParam( NEEDBAR_OFFSETX, NEEDBAR_OFFSETY+NEEDVAR_STEPY);
	playElements.push(bar);
	playElements.push(lab);
	bar_energy = bar;
	
	lab = Crafty.e("2D, DOM, Text")
		.attr({ x: NEEDBAR_TEXT_X+NEEDBAR_STEPX, y: NEEDBAR_TEXT_Y+NEEDVAR_STEPY, z:Z_UI_ITEM+1})
		.textColor(RGB_UI_TEXT)
		.text(S_BAR_THIRST)
	bar = Crafty.e("NeedBar");
	bar.setParam( NEEDBAR_OFFSETX+NEEDBAR_STEPX, NEEDBAR_OFFSETY+NEEDVAR_STEPY);
	playElements.push(bar);
	playElements.push(lab);
	bar_temp = bar;

	lab = Crafty.e("2D, DOM, Text")
		.attr({ x: NEEDBAR_TEXT_X, y: NEEDBAR_TEXT_Y+2*NEEDVAR_STEPY, z:Z_UI_ITEM+1})
		.textColor(RGB_UI_TEXT)
		.text(S_BAR_FUN)
	bar = Crafty.e("NeedBar");
	bar.setParam( NEEDBAR_OFFSETX, NEEDBAR_OFFSETY+2*NEEDVAR_STEPY);
	playElements.push(bar);
	playElements.push(lab);
	bar_energy = bar;
	
	lab = Crafty.e("2D, DOM, Text")
		.attr({ x: NEEDBAR_TEXT_X+NEEDBAR_STEPX, y: NEEDBAR_TEXT_Y+2*NEEDVAR_STEPY, z:Z_UI_ITEM+1})
		.textColor(RGB_UI_TEXT)
		.text(S_BAR_SOCIAL)
	bar = Crafty.e("NeedBar");
	bar.setParam( NEEDBAR_OFFSETX+NEEDBAR_STEPX, NEEDBAR_OFFSETY+2*NEEDVAR_STEPY);
	playElements.push(bar);
	playElements.push(lab);
	bar_temp = bar;
	
	lab = Crafty.e("2D, DOM, Text")
		.attr({ x: NEEDBAR_TEXT_X, y: NEEDBAR_TEXT_Y+3*NEEDVAR_STEPY, z:Z_UI_ITEM+1})
		.textColor(RGB_UI_TEXT)
		.text(S_BAR_WC)
	bar = Crafty.e("NeedBar");
	bar.setParam( NEEDBAR_OFFSETX, NEEDBAR_OFFSETY+3*NEEDVAR_STEPY);
	playElements.push(bar);
	playElements.push(lab);
	bar_energy = bar;
	
	lab = Crafty.e("2D, DOM, Text")
		.attr({ x: NEEDBAR_TEXT_X+NEEDBAR_STEPX, y: NEEDBAR_TEXT_Y+3*NEEDVAR_STEPY, z:Z_UI_ITEM+1})
		.textColor(RGB_UI_TEXT)
		.text(S_BAR_HEALTH)
	bar = Crafty.e("NeedBar");
	bar.setParam( NEEDBAR_OFFSETX+NEEDBAR_STEPX, NEEDBAR_OFFSETY+3*NEEDVAR_STEPY);
	playElements.push(bar);
	playElements.push(lab);
	bar_temp = bar;
}

var bar_hunger;
var bar_thirst;
var bar_fun;
var bar_social;
var bar_wc;
var bar_health;
var bar_energy;
var bar_temp;


function leavePlayingUI() {
	for( i=0; i<playElements.length; i++)
		playElements[i].destroy();
	playElements = [];
}
///////////////////////////////////////////////////////////////////////////


/////////////////////////Set Shopping UI///////////////////////////////////
var shopElements = new Array();
var itemsInCategory = new Array();

var active_category = "home";
	
var activeShopItem = 0;

var itemToPlace = null;
var placeHighlight = null;
var mouseListener;

var ITEMS_OFFSETX = 290;
var ITEMS_OFFSETY = WINDOW_H-96*0.75;
var ITEMS_STEPX = 20;
var ITEMS_STEPY = 20;

function setShoppingUI() {

	cur_sub_UI = "shop";

	activeShopItem = 0;

	var _text_w = 60;
	var _text_h = 15;

	var ent = Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: 215-_text_w/2, y: WINDOW_H-96+6, z: Z_UI_ITEM})
		.text(S_SHOP_CATEGORIES)
		.textFont({ family: 'Times New Roman' })
		.textColor(RGB_UI_TEXT)
		.css({"text-align": "center"})
		.textFont({ size: '12px', weight: 'bold' })
		.unselectable();
	shopElements.push(ent);

	ent = Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: 335-_text_w/2, y: WINDOW_H-96+6, z: Z_UI_ITEM})
		.text(S_SHOP_ITEMS)
		.textFont({ family: 'Times New Roman' })
		.textColor(RGB_UI_TEXT)
		.css({"text-align": "center"})
		.textFont({ size: '12px', weight: 'bold' })
		.unselectable();
	shopElements.push(ent);
	
	//categories
	var CAT_OFFSETX = 168;
	var CAT_OFFSETY = WINDOW_H-96+96*0.3;
	var CAT_STEPX = 25;
	var CAR_STEPY = 30;
	var cat = Crafty.e("2D, DOM, Mouse, sp_category_home")
		.attr({ x: CAT_OFFSETX, y: CAT_OFFSETY, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			this.sprite(3,0,1,1);
		}).bind("MouseOut", function() {
			if(active_category!="home")
				this.sprite(2,0,1,1);
		}).bind("Click", function() {
			this.sprite(3,0,1,1);
			active_category = "home";
			Crafty.trigger("CategoryBtnClicked");
		}).bind("CategoryBtnClicked", function() {
			if(active_category!="home") {
				//deactivate
				this.sprite(2,0,1,1);
			}
			activeShopItem = -1; //none
			if(itemToPlace!=null)
				itemToPlace.destroy();
			itemToPlace = null;
			if(placeHighlight!=null)
				placeHighlight.destroy();
			placeHighlight = null;
			loadNewCategory();
		});
	if(active_category=="home")
		cat.sprite(3,0,1,1);
	shopElements.push(cat);
		
	cat = Crafty.e("2D, DOM, Mouse, sp_category_food")
		.attr({ x: CAT_OFFSETX+CAT_STEPX, y: CAT_OFFSETY, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			this.sprite(1,0,1,1);
		}).bind("MouseOut", function() {
			if(active_category!="food")
				this.sprite(0,0,1,1);
		}).bind("Click", function() {
			this.sprite(1,0,1,1);
			active_category = "food";
			Crafty.trigger("CategoryBtnClicked");
		}).bind("CategoryBtnClicked", function() {
			if(active_category!="food") {
				//deactivate
				this.sprite(0,0,1,1);
			}
		});
	if(active_category=="food")
		cat.sprite(1,0,1,1);
	shopElements.push(cat);

	cat = Crafty.e("2D, DOM, Mouse, sp_category_fun")
		.attr({ x: CAT_OFFSETX+2*CAT_STEPX, y: CAT_OFFSETY, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			this.sprite(1,1,1,1);
		}).bind("MouseOut", function() {
			if(active_category!="fun")
				this.sprite(0,1,1,1);
		}).bind("Click", function() {
			this.sprite(1,1,1,1);
			active_category = "fun";
			Crafty.trigger("CategoryBtnClicked");
		}).bind("CategoryBtnClicked", function() {
			if(active_category!="fun") {
				//deactivate
				this.sprite(0,1,1,1);
			}
		});
	if(active_category=="fun")
		cat.sprite(1,1,1,1);
	shopElements.push(cat);
		
	cat = Crafty.e("2D, DOM, Mouse, sp_category_health")
		.attr({ x: CAT_OFFSETX+3*CAT_STEPX, y: CAT_OFFSETY, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			this.sprite(3,1,1,1);
		}).bind("MouseOut", function() {
			if(active_category!="health")
				this.sprite(2,1,1,1);
		}).bind("Click", function() {
			this.sprite(3,1,1,1);
			active_category = "health";
			Crafty.trigger("CategoryBtnClicked");
		}).bind("CategoryBtnClicked", function() {
			if(active_category!="health") {
				//deactivate
				this.sprite(2,1,1,1);
			}
		});
	if(active_category=="health")
		cat.sprite(3,1,1,1);
	shopElements.push(cat);

		
	loadNewCategory();

	mouseListener = Crafty.e("2D");
	mouseListener.onMouseMove = function(e) {
		if(itemToPlace!=null) {
			//snap to grid
			var _cTX = Math.round((e.realX-8)/16);
			var _cTY = Math.round((e.realY-8)/16);
			if(_cTX >= 0 && _cTX <25 && _cTY >= 0 && _cTY < 14) {
				itemToPlace.attr({ x: _cTX*16, y: _cTY*16, alpha: 1.0});
				if(itemToPlace.hit("Solid")) {
					if(placeHighlight.sp_red != true) {
						placeHighlight.sprite(1,0,1,1);
						placeHighlight.sp_red = true;
					}
				}
				else {
					if(placeHighlight.sp_red == true) {
						placeHighlight.sprite(2,0,1,1);
						placeHighlight.sp_red = false;
					}
				}
				placeHighlight.attr({ x: _cTX*16, y: _cTY*16, alpha: 1.0});
			}
				
		}
//		console.log(e.realX, e.realY);
	};
	Crafty.addEvent(mouseListener, Crafty.stage.elem, "mousemove", mouseListener.onMouseMove);
}

function leaveCategory() {
	for( i=0; i<itemsInCategory.length; i++) {
		itemsInCategory[i].destroy();
	}
	itemsInCategory = [];
}

function loadCategryFood() {

	var e = Crafty.e("ShopItem");
		e.setParam( 10, ITEMS_OFFSETX, ITEMS_OFFSETY, Z_ABOVE_GROUND, "sp_bowl_full", 0);
		e.addText(S_BOWL);
		
	//Crafty.e("ShopItem")
	//	.setParam( 10, ITEMS_OFFSETX+ITEMS_STEPX, ITEMS_OFFSETY, Z_ABOVE_GROUND, "sp_bowl_empty", 1);
}

function loadCategoryHome() {

	var e = Crafty.e("ShopItem");
	e.setParam( 20, ITEMS_OFFSETX, ITEMS_OFFSETY, Z_USEABLE_ITEM, "sp_doghouse", 0);
	e.addText(S_DOGHOUSE);
}

function loadCategoryFun() {

	var e = Crafty.e("ShopItem");
		e.setParam( 5, ITEMS_OFFSETX, ITEMS_OFFSETY, Z_ABOVE_GROUND, "sp_stick", 0);
		e.addText(S_STICK);
}

function loadCategoryHealth() {

	var e = Crafty.e("ShopItem");
		e.setParam( 15, ITEMS_OFFSETX, ITEMS_OFFSETY, Z_OBSTACLES, "sp_fire1", 0);
		e.addText(S_FIRE);
}

function loadNewCategory() {
	leaveCategory();
	if(active_category=="food") {
		loadCategryFood();
	} else if(active_category=="home") {
		loadCategoryHome();
	} else if(active_category=="fun") {
		loadCategoryFun();
	} else if(active_category=="health") {
		loadCategoryHealth();
	}
}

function leaveShoppingUI() {
	for( i=0; i<shopElements.length; i++) {
		shopElements[i].destroy();
	}
	shopElements = [];
	Crafty.removeEvent(mouseListener, Crafty.stage.elem, "mousemove", mouseListener.onMouseMove);
	if(itemToPlace!=null)
		itemToPlace.destroy();
	itemToPlace = null;
	if(placeHighlight!=null)
		placeHighlight.destroy();
	placeHighlight = null;
	
	leaveCategory();
}
///////////////////////////////////////////////////////////////////////////////////////

function updateGold() {
	playerGoldLabel.text(playerGold.toString());
}

////////////////////////////////ShopItem component///////////////////////////////////
//TT is for Tooltip
var SHOP_TT_OFFSETX = 97;
var SHOP_TT_OFFSETY = WINDOW_H-90;
var SHOP_TT_TEXTX = 5;
var SHOP_TT_TEXTY = 5;
var SHOP_TT_STEPY = 15;

var SHOP_TT_WIDTH = 60;
var SHOP_TT_HEIGHT = 65;
Crafty.c("ShopItem", {
	
	setParam: function( price, px, py, pz, c_sprite, item_number) {
	
		this.infoArray = new Array();
	
		this.price = price;
	
		var icon_border = Crafty.e("2D, DOM, Color")
			.attr({ x: px-1, y: py-1, z: Z_UI_ITEM-2, w: 18, h: 18})
			.color('#000000');
		itemsInCategory.push(icon_border);

		var icon_backgnd = Crafty.e("2D, DOM, Color")
			.attr({ x: px, y: py, z: Z_UI_ITEM-1, w: 16, h: 16})
			.color('#80d4df');
		itemsInCategory.push(icon_backgnd);
	
		this.itemNo = item_number;
		this.addComponent("2D, DOM, Mouse")
		.addComponent(c_sprite)
		.attr({ x: px, y: py, z: Z_UI_ITEM})
		.bind("MouseOver", function() {
			icon_border.color('#FF0000');
			
			this.tooltipE = Crafty.e("2D, DOM, Color, Tween")
				.attr({ x: SHOP_TT_OFFSETX, y: SHOP_TT_OFFSETY, z: Z_UI_ITEM, w: SHOP_TT_WIDTH, h: 0})
				.color('#FFFFC2')
				.tween({ h: SHOP_TT_HEIGHT}, 500)
				.bind("TweenEnd", function() {
					this.infos = new Array();
					var tx;
					
					tx = Crafty.e("2D, DOM, Text")
						.attr({ x: SHOP_TT_OFFSETX+SHOP_TT_TEXTX, y: SHOP_TT_OFFSETY+SHOP_TT_TEXTY, z: Z_UI_ITEM+1})
						.textColor('#000000')
						.text(S_TOOLTIP);
					this.infos.push(tx);
					
					for(i=0; i<this.infoArray.length; i++) {
						tx = Crafty.e("2D, DOM, Text")
							.attr({ x: SHOP_TT_OFFSETX+SHOP_TT_TEXTX, y: SHOP_TT_OFFSETY+SHOP_TT_TEXTY+(i+1)*SHOP_TT_STEPY, z: Z_UI_ITEM+1})
							.textColor('#000000')
							.text(this.infoArray[i]);
							
						this.infos.push(tx);
					}
					
					tx = Crafty.e("2D, DOM, Text")
						.attr({ x: SHOP_TT_OFFSETX+SHOP_TT_TEXTX, y: SHOP_TT_OFFSETY+SHOP_TT_TEXTY+(i+1)*SHOP_TT_STEPY, z: Z_UI_ITEM+1, w: SHOP_TT_WIDTH-2*SHOP_TT_TEXTX })
						.textColor('#000000')
						.text(S_COST + " " + this.price.toString() + "g");
						
					this.infos.push(tx);

				})
				.bind("Remove", function() {
					if(this.infos != null) {
						for(i=0; i<this.infos.length; i++)
							this.infos[i].destroy();
					}
				});
				
			this.tooltipE.infoArray = this.infoArray;
			this.tooltipE.price = price;
				
		})
		.bind("MouseOut", function() {
			if( this.itemNo != activeShopItem)
				icon_border.color('#000000');
			this.tooltipE.destroy();
		})
		.bind("Click", function() {
			activeShopItem = this.itemNo;
			icon_border.color('#FF0000');
			Crafty.trigger("ShopItemClick");
			if(itemToPlace!=null)
				itemToPlace.destroy();
			if(placeHighlight!=null)
				placeHighlight.destroy();
			itemToPlace = Crafty.e("2D, DOM, Collision, Solid, Usable").attr({z: pz, alpha: 0.0}).collision();
			itemToPlace.addComponent(c_sprite);
			itemToPlace.price = this.price;
			placeHighlight = Crafty.e("2D, DOM, sp_sel_red").attr({ z: Z_SELECTION, alpha: 0.0});
			placeHighlight.sp_red = true;
		})
		.bind("ShopItemClick", function() {
			if( this.itemNo != activeShopItem)
				icon_border.color('#000000');
		});
		itemsInCategory.push(this);
	},
	addText: function( tx ) {
		this.infoArray.push(tx);
	}
});
/////////////////////////////////////////////////////////////////////////////////////
///////////////////////////Action Menu///////////////////////////////////////////////
Crafty.c("ActionMenu", {
	init: function() {
		//this.addComponent("2D, DOM, ");
	},
	setParam: function( px, py, size, text, ID) {
		this.mID = ID;
		//put middle
			Crafty.e("ActionMenuComp")
			.setParam( px-8, py-8, "mid", ID);
		//put mid to the left
		for(i=0; i<(size-1)/2; i++) {
			Crafty.e("ActionMenuComp")
			.setParam( px-8-(i+1)*16, py-8, "mid", ID);
		}
		Crafty.e("ActionMenuComp")
		.setParam( px-8-(i+1)*16, py-8, "left", ID);
		//put mid to the right
		for(i=0; i<(size-1)/2; i++) {
			Crafty.e("ActionMenuComp")
			.setParam( px-8+(i+1)*16, py-8, "mid", ID);
		}
		Crafty.e("ActionMenuComp")
		.setParam( px-8+(i+1)*16, py-8, "right", ID);
		
		var t_sizeX = size*16;
		var t_sizeY = 16;
		Crafty.e("2D, DOM, Text")
			.attr({ x: px-t_sizeX/2, y: py-t_sizeY/2, z: Z_UI_ITEM+1, w: t_sizeX, h: t_sizeY})
			.text(text)
			.textColor(RGB_AMENU_TEXT_COLOR)
			.textFont({ size: '11px'})
			.css({"text-align": "center"})
			.bind("ActionMenuClick" + this.mID.toString(), function() {
				this.destroy();
			});
			
		this.bind("ActionMenuClick" + this.mID.toString(), function() {
			console.log("Click!", this.mID);
			//Later: destroy at TweenEnd?
			this.destroy();
		})
		this.bind("ActionMenuDestroy", function() {
			Crafty.trigger("ActionMenuClick" + this.mID.toString());
		});
	}
});

Crafty.c("ActionMenuComp", {
	init: function() {
		this.addComponent("2D, DOM, Mouse");
	},
	setParam: function( px, py, type, ID) {
	
		this.mID = ID;
	
		this.attr({ x: px, y: py, z: Z_UI_ITEM});
	
		if(type=="left") {
			this.sprite_D = "sp_Amenu_left";
			this.sprite_A = "sp_Amenu_left_A";
		}
		else if(type=="right") {
			this.sprite_D = "sp_Amenu_right";
			this.sprite_A = "sp_Amenu_right_A";
		}
		else { //if(type=="mid") {
			this.sprite_D = "sp_Amenu";
			this.sprite_A = "sp_Amenu_A";
		}
		
		this.addComponent(this.sprite_D);
		
		this.bind("MouseOver", function() {
			if(this.has(this.sprite_D))
				this.removeComponent(this.sprite_D);
			this.addComponent(this.sprite_A);
			Crafty.trigger("ActionMenuOver" + this.mID.toString());
		})
		.bind("MouseOut", function() {
			if(this.has(this.sprite_A))
				this.removeComponent(this.sprite_A);
			this.addComponent(this.sprite_D);
			Crafty.trigger("ActionMenuOut" + this.mID.toString());
		}).bind("ActionMenuOver" + this.mID.toString(), function() {
			if(this.has(this.sprite_D))
				this.removeComponent(this.sprite_D);
			this.addComponent(this.sprite_A);
		}).bind("ActionMenuOut" + this.mID.toString(), function() {
			if(this.has(this.sprite_A))
				this.removeComponent(this.sprite_A);
			this.addComponent(this.sprite_D);
		}).bind("Click", function() {
			Crafty.trigger("ActionMenuClick" + this.mID.toString());
		}).bind("ActionMenuClick" + this.mID.toString(), function() {
			this.destroy();
		})
	}
});

/////////////////////////////////////////////////////////////////////////////////////