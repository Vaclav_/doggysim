Crafty.scene("MainMenu", function() {
	
	Crafty.e("2D, DOM, Color")
		.attr({w: WINDOW_W, h: WINDOW_H, x: 0, y: 0})
		.color(RGB_MENU_BASE);
		
	var btn_w = 120;
	var btn_h = 25;
	
	//Tutorial
	Crafty.e("2D, DOM, Mouse, Color")
		.attr({w: btn_w, h: btn_h, x: WINDOW_W/2-btn_w/2, y: WINDOW_H*0.35-btn_h/2})
		.color(RGB_BUTTON_DEFAULT)
		.bind("Click", function() {
			if(!NEW_GAME) {
			}
		});
		
	//New game
	Crafty.e("2D, DOM, Mouse, Color")
		.attr({w: btn_w, h: btn_h, x: WINDOW_W/2-btn_w/2, y: WINDOW_H*0.5-btn_h/2})
		.color(RGB_BUTTON_DEFAULT)
		.bind("Click", function() {
			Crafty.scene("CreateADog");
		})
		.bind("MouseOver", function(){
			this.color(RGB_BUTTON_ACTIVE);
		})
		.bind("MouseOut", function(){
			this.color(RGB_BUTTON_DEFAULT);
		});
		
	//Save
	var save_btn = Crafty.e("2D, DOM, Mouse, Color")
		.attr({w: btn_w, h: btn_h, x: WINDOW_W/2-btn_w/2, y: WINDOW_H*0.65-btn_h/2})
		.color(RGB_BUTTON_DEFAULT)
		.bind("Click", function() {
			if(!NEW_GAME) {
			}
		});
	
	//Resume
	var resume_btn = Crafty.e("2D, DOM, Mouse, Color")
		.attr({w: btn_w, h: btn_h, x: WINDOW_W/2-btn_w/2, y: WINDOW_H*0.8-btn_h/2})
		.color(RGB_BUTTON_DEFAULT)
		.bind("Click", function() {
		});
		
	var _text_w = 150;
	var _text_h = 25;
	
	//"Shadow"
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: 25, x: WINDOW_W/2-_text_w/2+2, y: WINDOW_H*0.15+2})
		.text(S_DOGGYSIM)
		.textColor(RGB_MENU_TITLE_SHADOW)
		.textFont({ size: '26px', weight: 'bold', family: 'Arial' })
		.css({"text-align": "center"})
		.unselectable();
	
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: 25, x: WINDOW_W/2-_text_w/2, y: WINDOW_H*0.15})
		.text(S_DOGGYSIM)
		.textColor(RGB_MENU_TITLE)
		.textFont({ size: '26px', weight: 'bold', family: 'Arial' })
		.css({"text-align": "center"})
		.unselectable();
	
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W/2-_text_w/2, y: WINDOW_H*0.35-_text_h/2+3})
		.text(S_TUTORIAL)
		.textColor(RGB_BUTTON_TEXT)
		.textFont({ size: '13px', weight: 'bold' })
		.css({"text-align": "center"})
		.unselectable();
	
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W/2-_text_w/2, y: WINDOW_H*0.5-_text_h/2+3})
		.text(S_NEW_GAME)
		.textColor(RGB_BUTTON_TEXT)
		.textFont({ size: '13px', weight: 'bold' })
		.css({"text-align": "center"})
		.unselectable();
		
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W/2-_text_w/2, y: WINDOW_H*0.65-_text_h/2+3})
		.text(S_SAVE)
		.textColor(RGB_BUTTON_TEXT)
		.textFont({ size: '13px', weight: 'bold' })
		.css({"text-align": "center"})
		.unselectable();

	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W/2-_text_w/2, y: WINDOW_H*0.8-_text_h/2+3})
		.text(S_RESUME)
		.textColor(RGB_BUTTON_TEXT)
		.textFont({ size: '13px', weight: 'bold' })
		.css({"text-align": "center"})
		.unselectable();
	
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: 25, x: WINDOW_W*0.05, y: WINDOW_H*0.9})
		.text(S_MADE_BY + "V\u00E1clav")
		.textColor(RGB_MENU_TEXT)
		.textFont({ size: '11px', weight: 'bold' })
		.unselectable();
		
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: 25, x: WINDOW_W*0.95-_text_w, y: WINDOW_H*0.9})
		.text(GAME_VERSION)
		.textColor(RGB_MENU_TEXT)
		.textFont({ size: '11px', weight: 'bold' })
		.css({"text-align": "right"})
		.unselectable();
	
	//Tutorial N/A
	Crafty.e("2D, DOM, Color")
		.attr({w: btn_w, h: btn_h, x: WINDOW_W/2-btn_w/2, y: WINDOW_H*0.35-btn_h/2, alpha: 0.5})
		.color('#000000');
	
	if(!NEW_GAME) {
		save_btn.bind("MouseOver", function(){
			this.color(RGB_BUTTON_ACTIVE);
		})
		.bind("MouseOut", function(){
			this.color(RGB_BUTTON_DEFAULT);
		});
		
		resume_btn.bind("MouseOver", function(){
			this.color(RGB_BUTTON_ACTIVE);
		})
		.bind("MouseOut", function(){
			this.color(RGB_BUTTON_DEFAULT);
		});
	}
	
	if(NEW_GAME) {

		Crafty.e("2D, DOM, Color")
			.attr({w: btn_w, h: btn_h, x: WINDOW_W/2-btn_w/2, y: WINDOW_H*0.65-btn_h/2, alpha: 0.5})
			.color('#000000');
			
			Crafty.e("2D, DOM, Color")
			.attr({w: btn_w, h: btn_h, x: WINDOW_W/2-btn_w/2, y: WINDOW_H*0.8-btn_h/2, alpha: 0.5})
			.color('#000000');
	}
});