
function completeNewItem(item) {
	console.log("Complete");
	if(item.has("sp_doghouse")) {
		item.addComponent('Mouse');
		item.bind("MouseOver", function() {
			if(cur_sub_UI=="play") {
				if(item.has("sp_doghouse"))
					this.removeComponent("sp_doghouse");
				this.addComponent("sp_doghouse_A");
			}
		})
		.bind("MouseOut", function() {
			if(item.has("sp_doghouse_A"))
				this.removeComponent("sp_doghouse_A")
			this.addComponent("sp_doghouse");
		})
		.bind("Click", function() {
			if(cur_sub_UI=="play") {
				Crafty.e("ActionMenu").setParam( this._x+8, this._y-24, 3, S_DO_SLEEP, 1);
				cur_sub_UI = "action";
			}
		})
	} else if(item.has('sp_fire1')) {
		item.addComponent("SpriteAnimation");
		item.reel( "burn", 600, 0, 1, 3).animate("burn", -1);
		item.addComponent("Mouse");
		item.bind("MouseOver", function() {
			if(cur_sub_UI=="play") {
				this.pauseAnimation();
				this.resetAnimation();
				//fire1_A
				this.sprite(3,1,1,1);
			}
		})
		.bind("MouseOut", function() {
			//fire1
			this.sprite(0,1,1,1);
			this.animate("burn", -1);
		})
		.bind("Click", function() {
			if(cur_sub_UI=="play") {
				Crafty.e("ActionMenu").setParam( this._x+8, this._y-24, 3, S_DO_WATCH, 1);
				cur_sub_UI = "action";
			}
		})
	} else if(item.has('sp_stick')) {
		item.addComponent('Mouse');
		item.bind("MouseOver", function() {
			if(cur_sub_UI=="play") {
				if(item.has("sp_stick"))
					this.removeComponent("sp_stick");
				this.addComponent("sp_stick_A");
			}
		})
		.bind("MouseOut", function() {
			if(item.has("sp_stick_A"))
				this.removeComponent("sp_stick_A")
			this.addComponent("sp_stick");
		})
		.bind("Click", function() {
			if(cur_sub_UI=="play") {
				Crafty.e("ActionMenu").setParam( this._x+8, this._y-24, 3, S_DO_PLAY, 1);
				cur_sub_UI = "action";
			}
		})
	} else if(item.has('sp_bowl_full')) {
		item.addComponent('Mouse');
		item.bind("MouseOver", function() {
			if(cur_sub_UI=="play") {
				if(item.has("sp_bowl_full"))
					this.removeComponent("sp_bowl_full");
				this.addComponent("sp_bowl_full_A");
			}
		})
		.bind("MouseOut", function() {
			if(item.has("sp_bowl_full_A"))
				this.removeComponent("sp_bowl_full_A")
			this.addComponent("sp_bowl_full");
		}).bind("Click", function() {
			if(cur_sub_UI=="play") {
				Crafty.e("ActionMenu").setParam( this._x+8, this._y-24, 3, S_DO_EAT, 1);
				cur_sub_UI = "action";
			}
		})
	}
}