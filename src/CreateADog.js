Crafty.scene("CreateADog", function() {
	
	Crafty.e("2D, DOM, Color")
		.attr({w: WINDOW_W, h: WINDOW_H, x: 0, y: 0})
		.color(RGB_MENU_BASE);

	var _text_w = 250;
	var _text_h = 25;
	
	var ROW_BASE_T = 0.15;
	var ROW_BASE = 0.35;
	var ROW_STEP = 0.1;
	var COL_BASE = 0.15;
	var COL_STEP = 0.2;
	
	var btn_w = 80;
	var btn_h = 25;
	
	function showError() {
		Crafty.e("2D, DOM, Mouse, Color")
			.attr({w: 150, h: btn_h, x: WINDOW_W*(COL_BASE+COL_STEP/2)-150/2, y: WINDOW_H*(ROW_BASE+4.5*ROW_STEP)-btn_h/2})
			.color(RGB_LABEL_ERROR);
			
		Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W*(COL_BASE+COL_STEP/2)-_text_w/2, y: WINDOW_H*(ROW_BASE+4.5*ROW_STEP)-_text_h/2+3})
			.text(S_CREATE_A_DOG_ERROR)
			.textColor(RGB_BUTTON_TEXT)
			.css({"text-align": "center"})
			.textFont({ size: '13px', weight: 'bold' })
			.unselectable();
	}
	
	var AGE_MIN = 1;
	var AGE_MAX = 12;
	
	//next btn
	Crafty.e("2D, DOM, Mouse, Color")
		.attr({w: btn_w, h: btn_h, x: WINDOW_W*(COL_BASE+3*COL_STEP)-btn_w/2, y: WINDOW_H*(ROW_BASE+4.5*ROW_STEP)-btn_h/2})
		.color(RGB_BUTTON_DEFAULT)
		.bind("Click", function() {
			var name_input = document.getElementById('name_input').value;
			if(name_input == "")
				showError();
			else {
				console.log(name_input);
				var select_breed = document.getElementById('select_breed').value;
				console.log(select_breed);
				var select_color = document.getElementById('select_color').value;
				console.log(select_color);
				var age_input = document.getElementById('age').value;
				if(age_input == "")
					showError();
				else if( age_input < AGE_MIN || age_input > AGE_MAX )
					showError();
				else {
					//continue
					console.log(age_input);
					Crafty.scene("GameLot");
				}
			}
		}).bind("MouseOver", function(){
			this.color(RGB_BUTTON_ACTIVE);
		})
		.bind("MouseOut", function(){
			this.color(RGB_BUTTON_DEFAULT);
		});
	
	//"Shadow"
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W/2-_text_w/2+2, y: WINDOW_H*ROW_BASE_T+2})
		.text(S_CREATE_A_DOG)
		.textColor(RGB_MENU_TITLE_SHADOW)
		.textFont({ size: '26px', weight: 'bold', family: 'Arial' })
		.css({"text-align": "center"})
		.unselectable();
	
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W/2-_text_w/2, y: WINDOW_H*ROW_BASE_T})
		.text(S_CREATE_A_DOG)
		.textColor(RGB_MENU_TITLE)
		.textFont({ size: '26px', weight: 'bold', family: 'Arial' })
		.css({"text-align": "center"})
		.unselectable();
	
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W*COL_BASE, y: WINDOW_H*(ROW_BASE)-_text_h/2})
		.text(S_NAME)
		.textColor(RGB_MENU_TEXT)
		.textFont({ size: '13px', weight: 'bold' })
		.unselectable();
	
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W*COL_BASE, y: WINDOW_H*(ROW_BASE+1*ROW_STEP)-_text_h/2})
		.text(S_TYPE)
		.textColor(RGB_MENU_TEXT)
		.textFont({ size: '13px', weight: 'bold' })
		.unselectable();
		
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W*COL_BASE, y: WINDOW_H*(ROW_BASE+2*ROW_STEP)-_text_h/2})
		.text(S_COLOR)
		.textColor(RGB_MENU_TEXT)
		.textFont({ size: '13px', weight: 'bold' })
		.unselectable();

	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W*COL_BASE, y: WINDOW_H*(ROW_BASE+3*ROW_STEP)-_text_h/2})
		.text(S_AGE)
		.textColor(RGB_MENU_TEXT)
		.textFont({ size: '13px', weight: 'bold' })
		.unselectable();
		
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: _text_h, x: WINDOW_W*(COL_BASE+3*COL_STEP)-_text_w/2, y: WINDOW_H*(ROW_BASE+4.5*ROW_STEP)-_text_h/2+3})
		.text(S_NEXT)
		.textColor(RGB_BUTTON_TEXT)
		.css({"text-align": "center"})
		.textFont({ size: '13px', weight: 'bold' })
		.unselectable();
	
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: 25, x: WINDOW_W*0.05, y: WINDOW_H*0.9})
		.text(S_MADE_BY + "V\u00E1clav")
		.textColor(RGB_MENU_TEXT)
		.textFont({ size: '11px', weight: 'bold' })
		.unselectable();
		
	Crafty.e("2D, DOM, Text").attr({w: _text_w, h: 25, x: WINDOW_W*0.95-_text_w, y: WINDOW_H*0.9})
		.text(GAME_VERSION)
		.textColor(RGB_MENU_TEXT)
		.textFont({ size: '11px', weight: 'bold' })
		.css({"text-align": "right"})
		.unselectable();
	
	var _inp_w = 100;
	var _inp_h = 20;
	
	Crafty.e("HTML")
	   .attr({x: WINDOW_W*(COL_BASE+COL_STEP), y: WINDOW_H*ROW_BASE-_inp_h/2-6, w: _inp_w, h: _inp_h})
	   .replace("<input type='text' id='name_input' value='Doggy' maxlength='16' size='16'>");
	
	Crafty.e("HTML")
	   .attr({x: WINDOW_W*(COL_BASE+COL_STEP), y: WINDOW_H*(ROW_BASE+ROW_STEP)-_inp_h/2-6, w: _inp_w, h: _inp_h})
	   .replace("<select id='select_breed'> <option value='1'>Type1</option><option value='2'>Type2</option></select>");
	
	Crafty.e("HTML")
	   .attr({x: WINDOW_W*(COL_BASE+COL_STEP), y: WINDOW_H*(ROW_BASE+2*ROW_STEP)-_inp_h/2-6, w: _inp_w, h: _inp_h})
	   .replace("<select id='select_color'> <option value='1'>Brown</option><option value='2'>Black</option></select>");
	
	Crafty.e("HTML")
	   .attr({x: WINDOW_W*(COL_BASE+COL_STEP), y: WINDOW_H*(ROW_BASE+3*ROW_STEP)-_inp_h/2-6, w: _inp_w, h: _inp_h})
	   .replace("<input type='number' id='age' value='2' min='1' max='12'>");
	

	/*
	//Tutorial N/A
	Crafty.e("2D, DOM, Color")
		.attr({w: btn_w, h: btn_h, x: WINDOW_W/2-btn_w/2, y: WINDOW_H*0.35-btn_h/2, alpha: 0.5})
		.color('#000000');
	
	if(!NEW_GAME) {
		save_btn.bind("MouseOver", function(){
			this.color(THEME_FRONT);
		})
		.bind("MouseOut", function(){
			this.color(THEME_FRONT_LIGHT);
		});
		
		resume_btn.bind("MouseOver", function(){
			this.color(THEME_FRONT);
		})
		.bind("MouseOut", function(){
			this.color(THEME_FRONT_LIGHT);
		});
	}
	
	if(NEW_GAME) {

		Crafty.e("2D, DOM, Color")
			.attr({w: btn_w, h: btn_h, x: WINDOW_W/2-btn_w/2, y: WINDOW_H*0.65-btn_h/2, alpha: 0.5})
			.color('#000000');
			
			Crafty.e("2D, DOM, Color")
			.attr({w: btn_w, h: btn_h, x: WINDOW_W/2-btn_w/2, y: WINDOW_H*0.8-btn_h/2, alpha: 0.5})
			.color('#000000');
	}
	*/
});