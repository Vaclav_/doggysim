////////////////////////Generate random obstacles///////////////////////////
function genObstacles() {
	console.log("GENERATE");
	
	var r;

	for(row=1; row<6; row++) {
		for(col=1; col<14; col++) {
			r = Crafty.math.randomInt(0, 75);
			if(!r)
				Crafty.e("2D, DOM, Collision, Solid, sp_rock").attr({x: col*16, y: row*16, z: Z_OBSTACLES}).collision();
			else {
				r = Crafty.math.randomInt(0, 75);
				if(!r)
					Crafty.e("2D, DOM, Collision, Solid, sp_littletree").attr({x: col*16, y: row*16, z: Z_OBSTACLES}).collision();
			}
		}
	}

	for(row=0; row<8; row++) {
		for(col=15; col<25; col++) {
			r = Crafty.math.randomInt(0, 75);
			if(!r)
				Crafty.e("2D, DOM, Collision, Solid, sp_rock").attr({x: col*16, y: row*16, z: Z_OBSTACLES}).collision();
			else {
				r = Crafty.math.randomInt(0, 75);
				if(!r)
					Crafty.e("2D, DOM, Collision, Solid, sp_littletree").attr({x: col*16, y: row*16, z: Z_OBSTACLES}).collision();
			}
		}
	}

}
////////////////////////////////////////////////////////////////////////////

var selected_dog;	

Crafty.scene("GameLot", function() {

	Crafty.sprite( 16, "img/dog0.png", {
			sp_dog_front: [0,0],
			sp_dog_front_A: [4,0],
			sp_dog_left: [0,1],
			sp_dog_left_A: [4,1],
			sp_dog_right: [0,2],
			sp_dog_right_A: [4,2],
			sp_dog_back: [0,3],
			sp_dog_back_A: [4,3]
		});
	
	Crafty.sprite( 16, "img/Effects0.1.png", {
			sp_selection: [0,0],
			sp_sel_red: [1,0],
			sp_sel_green: [2,0],
			sp_Amenu_left: [0,1],
			sp_Amenu: [1,1],
			sp_Amenu_right: [2,1],
			sp_Amenu_left_A: [3,1],
			sp_Amenu_A: [4,1],
			sp_Amenu_right_A: [5,1]
		});


	Crafty.sprite( 16, "img/Obstacles.png", {
			sp_rock: [3,0],
			sp_littletree: [3,1]
		});
		
		//Test
		//Crafty.e("2D, DOM, Collision, Solid, sp_doghouse").attr({x: 32, y: 32, z: Z_USEABLE_ITEM, alpha: 1.0}).collision();

//////////////////////////////LOADING///////////////////////////////////
	// Black background with some loading text
	var black_screen = Crafty.e("2D, DOM, Color")
		.attr({w: WINDOW_W, h: WINDOW_H, x: 0, y: 0, z: Z_LOADING})
		.color("#000000");
	
	var loading_text = Crafty.e("2D, DOM, Text, Collision").attr({w: 100, h: 20, x: 150, y: 120, z: Z_LOADING})
		.text("Loading")
		.textColor('#FFFFFF')
		.css({"text-align": "center"});

	Crafty.load(["img/UI_Panel0.1.png", "img/UI_Icons0.12.png", "img/dog0.png", "img/Items0.1.png", "img/Effects0.1.png"], function() {
		Crafty.e("2D, DOM, TiledMapBuilder").setMapDataSource( MAP_LOT0_SRC )
			.createWorld( function( tiledmap ) {
				loadUI();

				for (var tile = 0; tile < tiledmap.getEntitiesInLayer('Tiles').length; tile++){
					tiledmap.getEntitiesInLayer('Tiles')[tile]
						//.addComponent("Tile")
						.attr({ z: Z_TILES });
				}
	
				for (var obstacle = 0; obstacle < tiledmap.getEntitiesInLayer('Obstacles').length; obstacle++){
					tiledmap.getEntitiesInLayer('Obstacles')[obstacle]
						.addComponent("Collision")
						.addComponent("Obstacle")
						.addComponent("Solid")
						.attr({ z: Z_OBSTACLES })
						.collision();
				}

				genObstacles();

				loading_text.destroy();
				black_screen.destroy();
			});
	});
////////////////////////////////////////////////////////////////////////////

//////////////////////////////GOBAL CLICK///////////////////////////////////
	console.log("register");
	var global_click_listener = Crafty.e("2D");
		global_click_listener.onMouseDown = function(e) {
			if(e.mouseButton == 0) {	//left click
				if( cur_sub_UI=="play" && e.realX>=0 && e.realX <=WINDOW_W-1 && e.realY >= 0 && e.realY <= WINDOW_H-1-96) {
					
					//Don't move onto interactive items
					var chechker = Crafty.e("2D, Collision").attr({x: e.realX, y: e.realY, w: 1, h: 1}).collision();
					if(!chechker.hit("Usable")) {

						var _cTX = Math.round((e.realX-8)/16);
						var _cTY = Math.round((e.realY-8)/16);
						console.log(_cTX,_cTY);
						
						if( Math.abs(_cTY-selected_dog.curTY) > Math.abs(_cTX-selected_dog.curTX) ) {
							selected_dog.finalTX = selected_dog.curTX;
							selected_dog.finalTY = _cTY;
						} else {
							selected_dog.finalTX = _cTX;
							selected_dog.finalTY = selected_dog.curTY;
						}
						Crafty.e("Selection_blink").create(selected_dog.finalTX,selected_dog.finalTY);
						if(!selected_dog.isMoving)
							selected_dog.getNextDest();
						
					}

				} else if( cur_sub_UI=="shop" && e.realX>=0 && e.realX <=WINDOW_W-1 && e.realY >= 0 && e.realY <= WINDOW_H-1-96) {
					//place/buy item!
					if( itemToPlace!=null && itemToPlace.price != null && itemToPlace.price <= playerGold ) {
						if( !itemToPlace.hit("Solid")) {
							
							playerGold -= itemToPlace.price;
							updateGold();
						
							var placed_item = itemToPlace.clone();
							//move it just to fix the bug in Crafty 0.6.2
							placed_item.x += 1;
							placed_item.x -= 1;
							placed_item.collision();
							
							completeNewItem(placed_item);
						}
					}
				} else if( cur_sub_UI == "action") {
					Crafty.trigger("ActionMenuDestroy");
					cur_sub_UI = "play";
				}
			}
		};

	Crafty.addEvent(global_click_listener, Crafty.stage.elem, "mousedown", global_click_listener.onMouseDown);
///////////////////////////////////////////////////////////////////////////

////////////////////////////////////FPS Counter////////////////////////////
/*
var renderTime = 0;
var frameTime = 0;
var waitTime = 0;
var cnt1 = 0;
var cnt2 = 0;
var cnt3 = 0;

Crafty.bind("MeasureRenderTime", function(t) { 
    cnt1++;
    renderTime += t;
});
Crafty.bind("MeasureFrameTime", function(t) { 
    frameTime += t;
    cnt2++;
});
Crafty.bind("MeasureWaitTime", function(t) { 
    waitTime += t;
    cnt3++;
});

Crafty.e("Delay").delay(function() {
    //console.log("RenderCnt: ", cnt1, " |FrameCnt: ", cnt2, " |WaitCnt: ", cnt3);
    console.log( "FPS: ", cnt1, "|Render: ", renderTime, " |Frame: ", frameTime, " |Wait: ", waitTime);
    cnt1 = 0;
    cnt2 = 0;
    cnt3 = 0;
    renderTime = 0;
    frameTime = 0;
    waitTime = 0;
}, 1000, -1);
*/
///////////////////////////////////////////////////////////////////////////

/////////////////////////////DOG///////////////////////////////////////////
	var DIR_FRONT = 1;
	var DIR_LEFT = 2;
	var DIR_RIGHT = 3;
	var DIR_BACK = 4;
	
	Crafty.c("Selection_blink", {
		init: function() {
			this.addComponent("2D, DOM, Tween, sp_selection");
		},
		create: function( tileX, tileY) {
			this.attr({x: tileX*16, y: tileY*16, z: Z_SELECTION, w:16, h:16});
			this.tween({alpha: 0.0}, 1000);
			this.bind("TweenEnd", function() {
				this.destroy();
			});
		}
	});
		
	Crafty.c("Dog", {
        init: function() {
            this.addComponent("2D, DOM, Collision, Mouse, SpriteAnimation, Solid, sp_dog_front");
			this.collision();
			this.speed = 1;
			this.isMoving = false;
			this.mouseOver = false;
			this.reel("run_front", 500, 0, 0, 3);
			this.reel("run_back", 500, 0, 3, 3);
			this.reel("run_left", 500, 0, 1, 3);
			this.reel("run_right", 500, 0, 2, 3);
			this.reel("run_front_A", 500, 3, 0, 3);
			this.reel("run_back_A", 500, 3, 3, 3);
			this.reel("run_left_A", 500, 3, 1, 3);
			this.reel("run_right_A", 500, 3, 2, 3);
			this.bind("DogArrive", function() {
				console.log("arrive");
				this.pauseAnimation();
				this.isMoving = false;
				this.updateSprite();
			})
			.bind("MouseOver", function() {
				if(cur_sub_UI=="play") {
					this.mouseOver = true;
					if(!this.isMoving) {
						this.updateSprite();
					} else {
						this.updateAnim();
					}
				}
			})
			.bind("MouseOut", function() {
				if(cur_sub_UI=="play") {
					this.mouseOver = false;
					if(!this.isMoving) {
						this.updateSprite();
					} else {
						this.updateAnim();
					}
				}
			})
			.bind('Moved', function(from) {
				if(this.hit('Solid')) {
					this.attr({x: from.x, y: from.y});
					this.trigger("DogArrive", null);
					this.isMoving = false;
				}
				if( this.x < 0 || this.x > WINDOW_W-16 ) {
					this.attr({x: from.x, y:from.y});
					this.isMoving = false;
				}
				if( this.y < 0 || this.y > WINDOW_H-96-16 ) {
					this.attr({x: from.x, y:from.y});
					this.isMoving = false;
				}
			})
			.bind('EnterFrame', function() {
				if(this.isMoving) {
					var _px = this.x;
					var _py = this.y;
					var add_sign;
					if(this.direction == DIR_LEFT || this.direction == DIR_BACK)
						add_sign = -1;
					else if(this.direction == DIR_RIGHT || this.direction == DIR_FRONT)
						add_sign = 1;
					else
						alert("Error: no such direction.");
					
					var scaled_speed = this.speed*G_SPEED_FACTOR;
					
					//horizontal
					if( this.direction == DIR_LEFT || this.direction == DIR_RIGHT) {
						if( Math.abs(this.x-this.nextTX*16) <= scaled_speed ) {
							this.curTX = this.nextTX;
							this.x = this.nextTX*16;
							this.getNextDest();
						} else {
							this.x += add_sign*scaled_speed;
						}
					} else { //vertical
						if( Math.abs(this.y-this.nextTY*16) <= scaled_speed ) {
							this.curTY = this.nextTY;
							this.y = this.nextTY*16;
							this.getNextDest();
						} else {
							this.y += add_sign*scaled_speed;
						}
					}
					this.trigger('Moved',  {x: _px, y: _py});
				}
			});
        },
		
		updateAnim: function() {
			var _cur_frame = this.currentFrame;
			if(_cur_frame==null)
				_cur_frame = 0;
			if(this.mouseOver) {
				switch(this.direction) {
					case DIR_FRONT:
						this.animate("run_front_A", -1);
						this.reelPosition(_cur_frame);
						break;
					case DIR_LEFT:
						this.animate("run_left_A", -1);
						this.reelPosition(_cur_frame);
						break;
					case DIR_RIGHT:
						this.animate("run_right_A", -1);
						this.reelPosition(_cur_frame);
						break;
					case DIR_BACK:
						this.animate("run_back_A", -1);
						this.reelPosition(_cur_frame);
						break;
					default:
						alert("Error: no such direction (event arrive");
				}
			} else {
				switch(this.direction) {
					case DIR_FRONT:
						this.animate("run_front", -1);
						this.reelPosition(_cur_frame);
						break;
					case DIR_LEFT:
						this.animate("run_left", -1);
						this.reelPosition(_cur_frame);
						break;
					case DIR_RIGHT:
						this.animate("run_right", -1);
						this.reelPosition(_cur_frame);
						break;
					case DIR_BACK:
						this.animate("run_back", -1);
						this.reelPosition(_cur_frame);
						break;
					default:
						alert("Error: no such direction (event arrive");
				}
			}
		},
		
		updateSprite: function() {
			if(this.mouseOver) {
				switch(this.direction) {
					case DIR_FRONT:
						this.sprite(3,0,1,1);
						break;
					case DIR_LEFT:
						this.sprite(3,1,1,1);
						break;
					case DIR_RIGHT:
						this.sprite(3,2,1,1);
						break;
					case DIR_BACK:
						this.sprite(3,3,1,1);
						break;
					default:
						alert("Error: no such direction (event arrive");
				}
			} else {
				switch(this.direction) {
					case DIR_FRONT:
						this.sprite(0,0,1,1);
						break;
					case DIR_LEFT:
						this.sprite(0,1,1,1);
						break;
					case DIR_RIGHT:
						this.sprite(0,2,1,1);
						break;
					case DIR_BACK:
						this.sprite(0,3,1,1);
						break;
					default:
						alert("Error: no such direction (event arrive");
				}
			}
		},
		
		playAnim: function() {
				switch(this.direction) {
					case DIR_FRONT:
						this.animate("run_front",-1);
						break;
					case DIR_LEFT:
						this.animate("run_left",-1);
						break;
					case DIR_RIGHT:
						this.animate("run_right",-1);
						break;
					case DIR_BACK:
						this.animate("run_back",-1);
						break;
					default:
						alert("Error: no such direction (playAnim)");
				}
		},
		getNextDest: function() {
			if(!this.isMoving)
				this.playAnim();
			this.isMoving = true;
			if(this.finalTX>this.curTX) {
				this.nextTX = this.curTX+1;
				if(this.direction!=DIR_RIGHT) {
					this.direction = DIR_RIGHT;
					this.playAnim();
				}
			}
			else if(this.finalTX<this.curTX) {
				this.nextTX = this.curTX-1;
				if(this.direction!=DIR_LEFT) {
					this.direction = DIR_LEFT;
					this.playAnim();
				}
			}
			else if(this.finalTY>this.curTY) {
				this.nextTY = this.curTY+1;
				if(this.direction!=DIR_FRONT) {
					this.direction = DIR_FRONT;
					this.playAnim();
				}
			}
			else if(this.finalTY<this.curTY) {
				this.nextTY = this.curTY-1;
				if(this.direction!=DIR_BACK) {
					this.direction = DIR_BACK;
					this.playAnim();
				}
			}
			else
				this.trigger("DogArrive", null);
		},
		
		Dog: function( posX, posY ) {
			this.attr({x: posX*16, y: posY*16, z: Z_CHARACTERS, w: 16, h:16});
			this.curTX = posX;
			this.curTY = posY;
			this.direction = DIR_FRONT;
			this.finalTX = this.curTX;
			this.finalTY = this.curTY;
			this.nextTX = this.curTX;
			this.nextTY = this.curTY;

		}
    });

	selected_dog = Crafty.e("Dog");
	selected_dog.Dog(6,9);
///////////////////////////////////////////////////////////////////////////

});
