//Works with crafty 0.6.2
window.onload = function() {
	
	//start crafty
	Crafty.init( WINDOW_W, WINDOW_H);
	
	Crafty.sprite( 90, 60, "img/Lang.png", {
		flag_en: [0,0],
		flag_en_active: [1,0],
		flag_hu: [2,0],
		flag_hu_active: [3,0]
	});
	
	/*
	// The loading screen that will display while our assets load
	Crafty.scene("Loading", function() {
	
		// Load takes an array of assets and a callback when complete
		 //when everything is loaded, run the main scene

		// Black background with some loading text
		Crafty.background("#000");
		Crafty.e("2D, DOM, Text").attr({w: 100, h: 20, x: 150, y: 120})
			.text("Loading")
			.textColor('#FFFFFF')
			.css({"text-align": "center"});
	});*/
  
	// Automatically play the loading scene
//	Crafty.scene("Loading");
	
	function toGame () {
		console.log( "Nyelv: " + GAME_LANG);
		//Load language file
		var script = document.createElement('script');
		var nowDate = new Date();
		if( GAME_LANG == "en") {
			script.src = "lang/lang-en.js" + "?nocache=" + nowDate.getTime();
			script.setAttribute('charset', 'utf-8');
			script.onload = function () {
				if(DEV_SKIP_MENU)
					Crafty.scene("GameLot");
				else
					Crafty.scene("MainMenu");
			};
		} else {
			script.src = "lang/lang-hu.js" + "?nocache=" + nowDate.getTime();
			script.setAttribute('charset', 'utf-8');
			script.onload = function () {
				if(DEV_SKIP_MENU)
					Crafty.scene("GameLot");
				else
					Crafty.scene("MainMenu");
			};
		} 
		document.head.appendChild(script);
	};
	
	Crafty.scene("LanguageSelect", function() {
		
		Crafty.e("2D, DOM, Color")
			.attr({w: WINDOW_W, h: WINDOW_H, x: 0, y: 0})
			.color(RGB_MENU_BASE)
			.bind('EnterFrame', function() {
				if(DEV_SKIP_MENU) {
					toGame();
					this.unbind('EnterFrame');
				}
			});
		
		var _text_w = 150;
		
		//"Shadow"
		Crafty.e("2D, DOM, Text").attr({w: _text_w, h: 25, x: WINDOW_W/2-_text_w/2+2, y: WINDOW_H*0.15+2})
			.text("DoggySim")
			.textColor(RGB_MENU_TITLE_SHADOW)
			.textFont({ size: '26px', weight: 'bold' })
			.css({"text-align": "center"})
			.unselectable();
		
		Crafty.e("2D, DOM, Text").attr({w: _text_w, h: 25, x: WINDOW_W/2-_text_w/2, y: WINDOW_H*0.15})
			.text("DoggySim")
			.textColor(RGB_MENU_TITLE)
			.textFont({ size: '26px', weight: 'bold' })
			.css({"text-align": "center"})
			.unselectable();
		
		Crafty.e("2D, DOM, Text").attr({w: _text_w, h: 25, x: WINDOW_W/2-_text_w/2, y: WINDOW_H*0.32})
			.text("Language | Nyelv")
			.textColor(RGB_MENU_TEXT)
			.textFont({ size: '13px', weight: 'bold' })
			.css({"text-align": "center"})
			.unselectable();
			
		Crafty.e("2D, DOM, Text").attr({w: _text_w, h: 25, x: WINDOW_W*0.05, y: WINDOW_H*0.9})
			.text("Made by V\u00E1clav")
			.textColor(RGB_MENU_TEXT)
			.textFont({ size: '11px', weight: 'bold' })
			.unselectable();
			
		Crafty.e("2D, DOM, Text").attr({w: _text_w, h: 25, x: WINDOW_W*0.95-_text_w, y: WINDOW_H*0.9})
			.text(GAME_VERSION)
			.textColor(RGB_MENU_TEXT)
			.textFont({ size: '11px', weight: 'bold' })
			.css({"text-align": "right"})
			.unselectable();
		
		Crafty.e("2D, DOM, SpriteAnimation, Mouse, flag_en")
			.attr({x: WINDOW_W/2-45-50, y: WINDOW_H*0.5-30})
			.reel( "inactive", 1000, 0, 0, 1)
			.reel( "active", 1000, 1, 0, 1)
			.bind("MouseOver", function() {
				this.animate("active", -1);
			})
			.bind("MouseOut", function() {
				this.animate("inactive", -1);
			})
			.bind("Click", function() {
				GAME_LANG = "en";
				toGame();
			});
			;

		Crafty.e("2D, DOM, SpriteAnimation, Mouse, flag_hu")
			.attr({x: WINDOW_W/2-45+50, y: WINDOW_H*0.5-30})
			.reel( "inactive", 1000, 2, 0, 1)
			.reel( "active", 1000, 3, 0, 1)
			.bind("MouseOver", function() {
				this.animate("active", -1);
			})
			.bind("MouseOut", function() {
				this.animate("inactive", -1);
			})
			.bind("Click", function() {
				GAME_LANG = "hu";
				toGame();
			})
			;
			
	});
	
	Crafty.scene("LanguageSelect");
	
};