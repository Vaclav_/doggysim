	
	var GAME_VERSION = "v0.09";
	
	var DEV_SKIP_MENU = false;
	
	var WINDOW_W = 400;
	var WINDOW_H = 320;
	
	var RGB_MENU_BASE = "#F0F8FF";
	var RGB_MENU_TITLE = "#000000";
	var RGB_MENU_TITLE_SHADOW = "#FFFFFF";
	var RGB_MENU_TEXT = "#000000";
	var RGB_BUTTON_TEXT = "#FFFFFF";
	var RGB_BUTTON_DEFAULT = "#80D4DF";
	var RGB_BUTTON_ACTIVE = "#51C5D4";
	var RGB_LABEL_ERROR = "#FF0000";
	var RGB_UI_TEXT = "#000000";
	var RGB_UI_BUTTON_DEFAULT = "#3333FF";
	var RGB_UI_BUTTON_ACTIVE = "#579EEC";
	var RGB_UI_BASE = '#579eec';
	//Action menu
	var RGB_AMENU_TEXT_COLOR = '#FFFFFF';
	
	var Z_TILES = 0;
	var Z_ON_GROUND = 5;
	var Z_SELECTION = 10;
	var Z_ABOVE_GROUND = 15;
	var Z_OBSTACLES = 25;
	var Z_USEABLE_ITEM = 30;
	var Z_CHARACTERS = 50;
	
	var Z_LOADING = 200;
	var Z_UI_PANEL = 100;
	var Z_UI_ITEM = 150;
	
	
	var GAME_LANG = "en";
	
	var NEW_GAME = true;
